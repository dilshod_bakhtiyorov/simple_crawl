import requests
from bs4 import BeautifulSoup


class SimpleCrawl:
    def __init__(self, pages):
        self.pages = pages

    def get_titles(self):
        titles = []
        for page in self.pages:
            url = page.history[0].url if page.history else page.url
            soup = BeautifulSoup(page.content, "html.parser")
            titles.append(
                {
                    'title': soup.find('title').text,
                    'url': url
                }
            )
        return titles



# x = SimpleCrawl(urls=['https://kun.uz/', 'https://www.udemy.com/'])
# print(x.get_titles())