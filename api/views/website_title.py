from rest_framework.views import APIView
from rest_framework.response import Response

from api.serializers.website_url import WebsiteUrlItemSerializer
from api.utils.title_crawl import SimpleCrawl


class SimpleCrawlAPIView(APIView):
    """
    + "urls" : **["https://kun.uz/", "https://www.udemy.com/"]**
    """
    def post(self, request):
        serializer = WebsiteUrlItemSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        pages = serializer.validated_data['pages']
        simple_crawl = SimpleCrawl(pages=pages)
        return Response(simple_crawl.get_titles())




