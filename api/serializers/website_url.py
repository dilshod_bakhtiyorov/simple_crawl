import requests
from rest_framework import serializers


class WebsiteUrlItemSerializer(serializers.Serializer):
    urls = serializers.ListField(write_only=True)
    pages = serializers.ListField(required=False, read_only=True)

    def validate(self, attrs):
        self._errors = []
        self._pages = []
        if attrs.get('urls'):
            for url in attrs.get('urls'):
                try:
                    page = requests.get(url)
                    self._pages.append(page)
                except:
                    self._errors.append({'URL_NOT_VALID': f"{url}"})
        attrs['pages'] = self._pages
        if self._errors:
            raise serializers.ValidationError(self._errors)
        return attrs
