from rest_framework.test import APIClient


def test_scaler_order_list():
    endpoint = "/api/v1/crawl/"
    client = APIClient()
    data = {
        "urls": [
            "https://kun.uz/",
            "https://www.udemy.com/",
            "https://ritm.uz/"
        ]
    }
    response = client.post(
        endpoint,
        data=data,
        format='json'
    )
    assert response.status_code == 200