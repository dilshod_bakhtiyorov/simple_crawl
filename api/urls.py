from django.urls import path

from api.views.website_title import SimpleCrawlAPIView

urlpatterns = [
    path('', SimpleCrawlAPIView.as_view(), name='simple_crawl'),
]
